package test2.question3;

import java.util.Objects;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet> {
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	
	/**
	 * This method overrides the original Java .equals() method to allow comparison
	 * between Planet objects. If the object is not even a planet, it immediately
	 * returns false.
	 * 
	 * @author Danilo Zhu 1943382
	 * @param o An object that is compared to planet.
	 * @return A boolean that indicates if the Planet objects are equal.
	 */
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (o instanceof Planet) {
			String objName = ((Planet) o).getName();
			int objOrder = ((Planet) o).getOrder();

			if (this.getName().equals(objName) && this.getRadius() == objOrder) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * This method overrides the hashCode() method.
	 * 
	 * @author Danilo Zhu 1943382
	 * @return A hashcode representing the object, depending on its name and order.
	 */
	@Override
	public int hashCode() {
		String toHash = this.getName() + this.getOrder();
		return toHash.hashCode();
	}

	/**
	 * This overrides the compareTo method to allow custom comparison between Planet
	 * objects.
	 * 
	 * @author Danilo Zhu 1943882
	 * @param A Planet object to which this is compared to.
	 * @return Returns a positive or negative int, depending on the outcome of the
	 *         comparison.
	 */
	@Override
	public int compareTo(Planet p) {
		int nameCompare = this.getName().compareTo(p.getName());
		if (nameCompare == 0) {
			return -1 * (this.getPlanetarySystemName().compareTo(p.getPlanetarySystemName()));
		}
		return nameCompare;
	}
}
