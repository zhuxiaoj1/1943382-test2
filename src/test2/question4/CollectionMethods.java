package test2.question4;

import java.util.*;
import test2.question3.Planet;

/**
 * This class contains the method getInnerPlanets which filters out outer
 * planets and returns a Collection<Planet>.
 * 
 * @author Danilo Zhu 1943382
 */
public class CollectionMethods {
	/**
	 * This method filters out the inner planets and returns them.
	 * 
	 * @param planets The original Collection of planets.
	 * @return	A Collection of inner planets.
	 */
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets) {
		ArrayList<Planet> innerPlanets = new ArrayList<Planet>();

		for (Planet i : planets) {
			switch (i.getOrder()) {
			case 1:
				innerPlanets.set(0, i);
				break;
			case 2:
				innerPlanets.set(1, i);
				break;
			case 3:
				innerPlanets.set(2, i);
				break;
			}
		}

		return (Collection<Planet>) innerPlanets;
	}
}
