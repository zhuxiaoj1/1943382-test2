package test2.question2;

/**
 * This class contains the constructor for an HourlyEmployee object.
 * 
 * @author Danilo Zhu 1943382
 */
public class HourlyEmployee implements Employee {
	protected int weeklyHours;
	protected double hourlyPay;

	/**
	 * Constructor for a HourlyEmployee object.
	 * 
	 * @param weeklyHours The number of hours worked per week.
	 * @param hourlyPay   The pay per hour of the employee.
	 */
	public HourlyEmployee(int weeklyHours, double hourlyPay) {
		this.weeklyHours = weeklyHours;
		this.hourlyPay = hourlyPay;
	}

	/**
	 * This method overrides getYearlyPay() from Employee.java and adapts it to an
	 * HourlyEmployee object.
	 * 
	 * @return A double representing the yearly salary.
	 */
	@Override
	public double getYearlyPay() {
		return weeklyHours * hourlyPay * 52;
	}
}
