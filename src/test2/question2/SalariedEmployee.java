package test2.question2;

import java.util.Objects;

/**
 * This class contains the constructor for an SalariedEmployee object.
 * 
 * @author Danilo Zhu 1943382
 */
public class SalariedEmployee implements Employee {
	private double yearlySalary;

	/**
	 * Constructor for a SalariedEmployee object.
	 * 
	 * @param yearlySalary A double that is the yearly salary of the
	 *                     SalariedEmployee object.
	 */
	public SalariedEmployee(double yearlySalary) {
		this.yearlySalary = yearlySalary;
	}

	/**
	 * This method overrides getYearlyPay() from Employee.java and adapts it to a
	 * SalariedEmployee object.
	 * 
	 * @return A double representing the yearly salary.
	 */
	@Override
	public double getYearlyPay() {
		return yearlySalary;
	}
}
