package test2.question2;

/**
 * This interface contains a method getYearlyPay which returns a double
 * representing the yearly pay for employees.
 * 
 * @author Danilo Zhu 1943382
 */
public interface Employee {
	public double getYearlyPay();
}
