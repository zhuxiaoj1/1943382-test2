package test2.question2;

/**
 * This class contains the constructor for an UnionizedHourlyEmployee object and
 * extends the HourlyEmployee class.
 * 
 * @author Danilo Zhu 1943382
 */
public class UnionizedHourlyEmployee extends HourlyEmployee {
	private double pensionContribution;

	/**
	 * Constructor for a UnionizedHourlyEmployee object, it makes use of the
	 * superconstructor.
	 * 
	 * @param weeklyHours The number of hours worked per week.
	 * @param hourlyPay   The pay per hour of the employee.
	 * @double pensionContribution The pension amount.
	 */
	public UnionizedHourlyEmployee(int weeklyHours, double hourlyPay, double pensionContribution) {
		super(weeklyHours, hourlyPay);
		this.pensionContribution = pensionContribution;
	}

	/**
	 * This method overrides getYearlyPay() from Employee.java and adapts it to an
	 * UnionizedHourlyEmployee object.
	 * 
	 * @return A double representing the yearly salary.
	 */
	@Override
	public double getYearlyPay() {
		return weeklyHours * hourlyPay * 52 + pensionContribution;
	}
}
