package test2.question2;

import java.util.Random;

/**
 * This class contains the main method and a method to calculate the yearlyPay
 * of employees.
 * 
 * @author Danilo Zhu 1943382
 */
public class PayrollManagement {
	/**
	 * The main method.
	 * 
	 * @param args Command-line arguments.
	 */
	public static void main(String[] args) {
		Random ra = new Random(); // Using random to generate weekly work hours and salaries.

		Employee[] employeeArr = new Employee[5];
		employeeArr[0] = new HourlyEmployee(ra.nextInt(28) + 20, ra.nextInt(10) + 12.75);
		employeeArr[1] = new UnionizedHourlyEmployee(ra.nextInt(28) + 20, ra.nextInt(10) + 12.75,
				ra.nextInt(3000) + 1000);
		employeeArr[2] = new SalariedEmployee(ra.nextInt(20000) + 40000);
		employeeArr[3] = new HourlyEmployee(ra.nextInt(28) + 20, ra.nextInt(10) + 12.75);
		employeeArr[4] = new UnionizedHourlyEmployee(ra.nextInt(28) + 20, ra.nextInt(10) + 12.75,
				ra.nextInt(3000) + 1000);

		System.out.println("The total expenses are: " + getTotalExpenses(employeeArr) + "$"); // Printing out the
																								// result.
	}

	/**
	 * This method calculates the total yearly expenses from an Employee array and
	 * returns it.
	 * 
	 * @param employees An employee array that contains all the employees.
	 * @return A double that represents the total expenses.
	 */
	public static double getTotalExpenses(Employee[] employees) {
		double expenses = 0.0;
		for (Employee i : employees) {
			expenses += i.getYearlyPay();
		}

		return expenses;
	}
}
